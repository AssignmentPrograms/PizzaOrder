


#ifndef PIZZAORDER_H
#define PIZZAORDER_H

#include <string>
#include <vector>
#include "Pizza.h"
using namespace std;


/* Single Pizza Order */
class PizzaOrder{
public:

	/* Default Constructor */
	PizzaOrder();

	/* Add pizza */
	void addPizza(const Pizza& newPizza);

	/* Calculate order */
	void calculateOrderPrice();

	/* Create Receipt */
	string pizzaOrderReceipt();


/* Private variables */
private:
	int price;
	int numPizzas;
	/* Vector for several pizzas */
	vector<Pizza> pizza_v;
};



#endif
