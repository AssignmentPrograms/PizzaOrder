/* File Name: PizzaOrder.cpp
 * Author: Madhav Gautam
 * Student ID: P428J884
 * Exam Number: 1
 */

/*
 * Pizza Maintains the information regarding a customers single Pizza.
 * The price of the pizza is updated here.
 * The receipt for the pizza is also created here.
 * Pizza ensures the information is valid and creates a pizza.
 */

#include "Pizza.h"

Pizza::Pizza(){
		size = 'M';
		price = 0;
        numToppings = 0;
}

int Pizza::getPrice(){
        return price;
}

		/*
	 	 *A static function to be used only in this file.
	 	 *Used to validate pizza information
	     */
static inline bool validatePizza(const int& numToppings, const char& size){
		bool result = true;

		/*
		 * 1=One layer of cheese (Default)
		 * 2=Two layers of cheese (Double)
		 * 3=Three layers of cheese (Triple)
		 */
        switch(numToppings)
		{
	            case 1:
		                break;
		    	case 2:
			        	break;
		        case 3:
			        	break;
		        default:
			        	result = false;
        }

		switch(size)
        {
				case 'S':
				case 's':
						break;
				case 'M':
				case 'm':
				        break;
				case 'L':
				case 'l':
						break;
				default:
						result = false;
        }

		return result;
}

/* Create a pizza with customer information. Values do not change */
bool Pizza::CreatePizza(const char& pizzaSize, const int& numPizzaToppings){
		/* Update values with customers input */
		size = pizzaSize;
		numToppings = numPizzaToppings;

		/* Ensure our pizza is valid */
		return validatePizza(numToppings, size);
}

/* Find pizza price */
void Pizza::calculatePizzaPrice(){
		/*
		 * if 1 topping... 0 dollars added
		 * if 2 toppings... 1 dollar added
		 * if 3 toppings... 2 dollars added
		 */
		price += numToppings - 1;

		/* Update price */
		switch(size)
		{
				case 'S':
				case 's':
						price += 6;
						break;
				case 'M':
				case 'm':
						price += 8;
						break;
				case 'L':
				case 'l':
						price += 10;
						break;
		}

}

/* Create individual Pizza Receipt */
string Pizza::pizzaReceipt(){
		/* Convert price to a string friendly type to be converted */
		char first_c = ' ';
		char second_c;
		if((price - 9) > 0){
				first_c = '1';
				second_c = ((price - 10) + '0');
		}
		else{
				second_c = price + '0';
		}

		string receipt = "PIZZA Receipt...\n";
		receipt += "You ordered a ";
		/* Dtermine size of pizza */
        switch(size)
		{
				case 'S':
				case 's':
						receipt += "Small Pizza";
						break;
				case 'M':
				case 'm':
						receipt += "Medium Pizza";
						break;
				case 'L':
				case 'l':
						receipt += "Large Pizza";
						break;
		}

		/* Determine which topping option was chosen */
		switch(numToppings)
		{
			case 1:
					receipt += " with Single topping.\n";
					break;
			case 2:
					receipt += " with Double toppings.\n";
					break;
			case 3:
					receipt += " with Tripple toppings.\n";
					break;
		}

		/* Print out the appropriate price */
		if(first_c != ' '){
				receipt += "The Price for this Pizza is $";
				receipt += first_c;
				receipt += second_c;
		}
		else{
				receipt += "The Price for this Pizza is: $";
				receipt += second_c;
		}

		/* Tack on user friendly cent information to match dollar format */
		receipt += ".00\n";
		return receipt;
}
