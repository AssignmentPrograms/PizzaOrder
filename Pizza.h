

#ifndef PIZZA_H
#define PIZZA_H

#include <string>
using namespace std;

/* Single Cheese Pizza */
class Pizza{
public:

		Pizza();

		/* Getter for data */
		int getPrice();

		/* Create a Pizza */
		bool CreatePizza(const char& pizzaSize, const int& numPizzaToppings);

		/* Find price or pizza */
		void calculatePizzaPrice();

		/* create a receipt for pizza */
		string pizzaReceipt();


/* Private variables for Pizza */
private:
		char size;
		int price;
		int numToppings;
};



#endif
