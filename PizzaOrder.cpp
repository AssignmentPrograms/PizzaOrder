/* File Name: PizzaOrder.cpp
 * Author: Madhav Gautam
 * Student ID: P428J884
 * Exam Number: 1
 */

/*
 * PizzaOrder Maintains the information regarding a customers order.
 * The price of the order is updated here.
 * The receipt for the order is also created here.
 * PizzaOrder stores each individual pizza in vector.
 */


#include "PizzaOrder.h"

/* Constructor */
PizzaOrder::PizzaOrder() {
		/* Initialize values for safety */
		price = 0;
		numPizzas = 0;
}

/* Add a new pizza */
void PizzaOrder::addPizza(const Pizza& newPizza) {
		/* Add pizza onto pizza vector */
		pizza_v.push_back(newPizza);
		/* new pizza in order */
		numPizzas++;
}

/* Find price of order */
void PizzaOrder::calculateOrderPrice() {
		/* Get each price of individuals */
		for(int i = 0; i < numPizzas; i++) {
				price += pizza_v[i].getPrice();
		}
}

/* Create the receipt for the order */
string PizzaOrder::pizzaOrderReceipt() {
		char first_c = ' ';
		char second_c;
		if ((price) > 29) {
				first_c = '3';
				second_c = ((price - 30) + '0');
		}
		else if ((price) > 19) {
				first_c = '2';
				second_c = ((price - 20) + '0');
		}
		else if ((price) > 9) {
				first_c = '1';
				second_c = ((price - 10) + '0');
		}
		else{
				second_c = price + '0';
		}

		char order_num;
		char numPizzas_c = numPizzas + '0';

		string receipt = "ORDER Receipt...\n";
		receipt += "You ordered ";
		receipt += numPizzas_c;
		receipt += " Pizza(s)\n";

		for (int i = 0; i < numPizzas; i++) {
				order_num = (i+1) + '0';
				receipt += "\nPizza Order #";
				receipt += order_num;
				receipt += "...\n\n";
				receipt += pizza_v[i].pizzaReceipt();
		}

		if (first_c != ' ') {
				receipt += "\nThe total price for the order is: $";
				receipt += first_c;
				receipt += second_c;
		}
		else {
				receipt += "\nThe total price for the order is: $";
				receipt += second_c;
		}

		/* Tack on user friendly cent information to match dollar format */
		receipt += ".00\n";
		return receipt;
}
