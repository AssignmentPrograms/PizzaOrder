/* File Name: PizzaOrderDemo.cpp
 * Author: Madhav Gautam
 * Student ID: P428J884
 * Exam Number: 1
 */

/*
 * PizzaOrderDemo is interface with customer.
 * Information for the order is obtained here.
 * The customer is prompted with choices to build order.
 * Receipt and Price shown to the user here.
 */

#include <iostream>
#include <string>

#include "Pizza.h"
#include "PizzaOrder.h"

using namespace std;

int main(){

	int numPizzas;

	/* Prompt for number of pizzas */
	cout << "How many Pizzas do you want?" << endl;
	cin >> numPizzas;

	/* Must be 1, 2 or 3 orders of pizza */
	if(numPizzas < 4 && numPizzas > 0){

		/* Store pizza information */
		char size;
		int numToppings;
		PizzaOrder pizzaOrder;

		// Determine if pizza is valid
		bool validPizza;

		/* For each pizza in order... */
		for(int i = 0; i < numPizzas; i++){
			/* The pizza object */
			Pizza pizza;

			/* Loop to ensure valid pizza before continuing */
			do
			{
				/* Assume valid pizza */
				validPizza = true;
				cout << "\nFor Pizza " << i+1 << " what size should we make it?" << endl;
				cout << "We offer Small(S), Medium(M) or Large(L)" << endl;
				cout << "Enter in 'S' for Small, 'M' for Medium or 'L' for Large\n";
				cout << "Size: ";
				cin >> size;
				cout << endl;

				cout << "Our Pizza's comes with a single free layer of cheese\n";
				cout << "What would you like to do with pizza " << i+1 << "?\n";
				cout << "Your options are..." << endl;
				cout << "1. Keep the single layer of cheese" << endl;
				cout << "2. Double the amount of cheese." << endl;
				cout << "3. Tripple the amount of cheese." << endl;
				cin >> numToppings;

				if(!pizza.CreatePizza(size, numToppings)){
					// if invalid...
					validPizza = false;
					cout << endl << endl;
					cout << "This Pizza is invalid please renter the information\n";
					cout << endl << endl;
				}

			}while( !validPizza );

			/* Find pizza price */
			pizza.calculatePizzaPrice();
			/* Add a pizza to order */
			pizzaOrder.addPizza(pizza);
			cout << endl;
		}

		/* Find order price */
		pizzaOrder.calculateOrderPrice();
		/* print order receipt and the order contents */
		cout << endl << pizzaOrder.pizzaOrderReceipt();
	}
	else{
		/* inform user why program may be ending due to invalid input */
		cout << "Not a valid number..." << endl;
	}

	cout << "Come Again!" << endl;
}
